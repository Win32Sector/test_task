#!/usr/bin/env bash

#update and install ansible 2.7
yum install -y epel-release
yum update -y
yum install -y ansible git curl
useradd -g wheel -m ansible
echo "ansible ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/ansible
